package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class PrintService {

	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";

		System.out.println();
		System.out.println(line);
		System.out.printf("%30s %n", title);
		System.out.println(line);

		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			} else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}

	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
		System.out.format(line);
		int number = 1;
		String vehicleType = "";
		for (Vehicle vehicle : listVehicle) {
			if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			} else {
				vehicleType = "Motor";
			}
			System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(),
					vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
			number++;
		}
		System.out.printf(line);
	}

	public static void printCustomerInfo(String title, Customer customer) {
		String formatTabel = "%-16s : %s %n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.println();
		System.out.format(line);
		System.out.printf("%65s\n", title);
		System.out.format(line);
		System.out.printf(formatTabel, "Customer Id", customer.getCustomerId());
		System.out.printf(formatTabel, "Nama", customer.getName());
		if (customer instanceof MemberCustomer) {
			System.out.printf(formatTabel, "Customer Status", "Member");
		} else {
			System.out.printf(formatTabel, "Customer Status", "Non Member");
		}

		System.out.printf(formatTabel, "Alamat", customer.getAddress());
		if (customer instanceof MemberCustomer) {
			String temp = String.format("%,d", (int) ((MemberCustomer) customer).getSaldoCoin());
			String newTemp = temp.replace(',', '.');
			System.out.printf(formatTabel, "Saldo Koin", newTemp);
		}
		System.out.println(
				"---------------------------------------------------------------------------------------------------");

		System.out.println("List Kendaraan :");
		printVechicle(customer.getVehicles());
		System.out.println();
	}

	// Silahkan Tambahkan function print sesuai dengan kebutuhan.
	public static void printItemServiceByVehicleType(String title, List<ItemService> listItemServices) {
		String formatTable = "| %-2s | %-15s | %-20s | %-17s | %-13s |%n";
		System.out.println();
		System.out.println(title);
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga");
		System.out.format(line);
		int number = 1;
		for (ItemService itemService : listItemServices) {
			System.out.printf(formatTable, number, itemService.getServiceId(), itemService.getServiceName(),
					itemService.getVehicleType(), itemService.getPrice());
			number++;
		}
		System.out.printf(line);
		System.out.println();
	}

	public static void printBookingOrderList(String title, List<BookingOrder> listBookingOrders, Customer customer) {
		String formatTable = "| %-2s | %-20s | %-20s | %-15s | %-15s | %-15s | %-30s |%n";
		System.out.println();
		System.out.println(title);
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+-----------------+---------------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Booking Id", "Nama Customer", "Payment Method", "Total Service",
				"Total Payment", "List Service");
		System.out.format(line);
		int number = 1;
		for (BookingOrder bookingOrder : listBookingOrders) {
			if (bookingOrder.getCustomer().equals(customer)) {
				System.out.printf(formatTable, number, bookingOrder.getBookingId(),
						bookingOrder.getCustomer().getName(),
						bookingOrder.getPaymentMethod(), formatCurrency(bookingOrder.getTotalServicePrice()),
						formatCurrency(bookingOrder.getTotalPayment()), printListService(bookingOrder.getServices()));
				number++;
			}
		}
		System.out.printf(line);
		System.out.println();
	}

	public static String printListService(List<ItemService> items) {
		String[] arr = new String[items.size()];
		for (int i = 0; i < items.size(); i++) {
			arr[i] = items.get(i).getServiceName();
		}
		String result = String.join(", ", arr);
		return result;
	}

	public static String formatCurrency(double value) {
		String result = "";
		String temp = String.format("%,d", (int) value);
		result = temp.replace(',', '.');
		return result;
	}
}
