package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
	private static List<BookingOrder> listAllBookingOrder = new ArrayList<BookingOrder>();
	private static Scanner input = new Scanner(System.in);
	private static Customer customerLog = null;

	public static void run() {
		boolean isLooping = true;
		// System.out.println("Aplikasi Booking Bengkel");
		String[] firstMenu = { "Login", "Exit" };
		int choice;
		if (customerLog == null) {
			do {
				PrintService.printMenu(firstMenu, "Aplikasi Booking Bengkel");
				choice = Validation.numberValidation("Silahkan Pilih Angka 1 untuk Login dan 0 untuk Exit : ",
						"Please input using number only ..!");
				switch (choice) {
					case 0:
						System.exit(0);
						break;
					case 1:
						login();
						// isLooping = false;
						break;
					default:
						System.out.println("Number input not found ..! \nPlease Input 1 for Login or 0 for Exit");
						run();
						break;
				}
				login();
				// mainMenu();
			} while (isLooping);
		}

	}

	public static void login() {
		String customerId, password;
		boolean isLogin = false;
		boolean isLooping = true;
		int count = 0;
		System.out.println();
		System.out.println("=============================================");
		System.out.println("                     LOGIN                   ");
		System.out.println("=============================================");
		do {
			customerId = Validation.customerValidationbyId("Masukan Customer ID : ", "Customer ID tidak ditemukan ..!",
					listAllCustomers);
			System.out.println("Masukan Password : ");
			password = input.nextLine();
			isLogin = BengkelService.login(customerId, password, listAllCustomers);
			if (isLogin == true) {
				customerLog = BengkelService.getCustomerbyId(customerId, listAllCustomers);
				mainMenu(customerLog);
				isLooping = false;
			} else {
				System.out.println();
				System.out.println();
				System.out.println("=============================================");
				System.out.println("Password yang dimasukkan salah..!");
				System.out.println("=============================================");
				System.out.println();
			}
			count++;
			if (count == 3) {
				System.out.println();
				System.out.println(
						"=========================================================================================================");
				System.out.println(
						"Anda Sudah Memasukan Password Yang Salah 3 Kali. Silahkan Hubungi Admin Bengkel Untuk Memulihkan Akun!");
				System.out.println(
						"=========================================================================================================");
				System.out.println();
				isLooping = false;
				MenuService.run();
			}
		} while (isLooping);

	}

	public static void mainMenu(Customer customer) {
		String[] listMenu = { "Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking",
				"Logout" };
		int menuChoice = 0;
		boolean isLooping = true;

		do {
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!",
					"^[0-9]+$", listMenu.length - 1, 0);
			// System.out.println(menuChoice);

			switch (menuChoice) {
				case 1:
					// panggil fitur Informasi Customer
					PrintService.printCustomerInfo("Informasi Profil Customer", customer);
					break;
				case 2:
					// panggil fitur Booking Bengkel
					BengkelService.bookingBengkel(customer, listAllItemService, listAllBookingOrder);
					break;
				case 3:
					// panggil fitur Top Up Saldo Coin
					if (customer instanceof MemberCustomer) {
						BengkelService.toUpSaldoKoin(customer);
					} else {
						System.out.println();
						System.out.println(
								"=============================================================================================");
						System.out.println(
								"              Mohon Maaf! Menu ini hanya dapat diakses oleh member bengkel saja!             ");
						System.out.println(
								"=============================================================================================");
						System.out.println();
					}
					break;
				case 4:
					PrintService.printBookingOrderList("Booking Order Menu", listAllBookingOrder, customer);
					// panggil fitur Informasi Booking Order
					break;
				default:
					System.out.println();
					System.out.println("=============================================");
					System.out.println("                    LOGOUT                   ");
					System.out.println("=============================================");
					System.out.println("\n");
					isLooping = false;
					customerLog = null;
					run();
					break;
			}
		} while (isLooping);

	}

	// Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi
}
