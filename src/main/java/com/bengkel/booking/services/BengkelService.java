package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class BengkelService {

	// Silahkan tambahkan fitur-fitur utama aplikasi disini
	// Get Customer by ID
	public static Customer getCustomerbyId(String id, List<Customer> customers) {
		Customer result = new Customer();
		for (Customer customer : customers) {
			if (customer.getCustomerId().equalsIgnoreCase(id)) {
				result = customer;
			}
		}
		return result;
	}

	// Login
	public static boolean login(String id, String password, List<Customer> customers) {
		boolean result = false;
		for (Customer customer : customers) {
			if (customer.getCustomerId().equalsIgnoreCase(id) && customer.getPassword().equalsIgnoreCase(password)) {
				result = true;
			}
		}
		return result;
	}

	// Info Customer(Tidak Dipakai)

	// Booking atau Reservation
	public static void bookingBengkel(Customer customer, List<ItemService> listItemService,
			List<BookingOrder> bookingOrder) {
		Vehicle vehicle = null;
		boolean isLooping = true;
		BookingOrder newBookingOrder = new BookingOrder();

		// Set List for saving data all item service that customer has been choosen
		List<ItemService> listChoosenItems = new ArrayList<ItemService>();

		// Generate variable for booking order
		String bookingID = generateBookingID(customer, bookingOrder);

		System.out.println();
		System.out.println("=============================================");
		System.out.println("              Booking Order Menu             ");
		System.out.println("=============================================");

		// Input Vehicle Id
		String vehicleId = Validation.vehicleValidationById("Masukan Vehicle Id : ", "Vehicle Id tidak ditemukan ..! ",
				customer.getVehicles());

		// Get Vehicle by vehicleId
		for (Vehicle getVehicle : customer.getVehicles()) {
			if (getVehicle.getVehiclesId().equalsIgnoreCase(vehicleId)) {
				vehicle = getVehicle;
			}
		}
		// Get Item Service for vehicle type
		List<ItemService> items = getItemServicesByTypeVehicle(vehicle.getVehicleType(),
				listItemService);

		// Show Item Service for vehicle
		PrintService.printItemServiceByVehicleType("List Service yang tersedia :", items);

		// Input Item Service for vehicle
		String serviceId = Validation.ItemServiceValidationById("Silahkan Masukan Service ID : ",
				"Service ID Tidak Ditemukan..!", items);
		ItemService itemServiceChooseByCust = getItemServicebyID(serviceId, items);
		listChoosenItems.add(itemServiceChooseByCust);
		if (customer instanceof MemberCustomer) {
			do {
				String select = Validation.validasiInput("Apakah anda ingin menambahkan Service Lainnya? (Y/T)",
						"Mohon Input Hanya Karakter \" Y \" atau \" T \" saja ! dapat berupa huruf besar atau kecil",
						"Y|T|y|t");

				switch (select.toUpperCase()) {
					case "Y":
						System.out.println();
						String serviceIdLainnya = Validation.ItemServiceValidationByIdLainnya(
								"Silahkan Masukan Service ID : ",
								"Service ID Tidak Ditemukan..!", items, listChoosenItems);
						ItemService itemServiceLainnyaChooseByCust = getItemServicebyID(serviceIdLainnya, items);
						listChoosenItems.add(itemServiceLainnyaChooseByCust);
						break;
					case "T":
						isLooping = false;
						break;

					default:
						break;
				}
				if (listChoosenItems.size() == 2) {
					System.out.println();
					System.out.println(
							"===================================================Catatan===========================================================");
					System.out.println(
							"Pilihan Service Anda Sudah Mencapai Batas Maksimal. Untuk Member Diberikan Batasan Maksimal Memilih Service Hanya 2x");
					System.out.println(
							"=====================================================================================================================");
					System.out.println();
					isLooping = false;
				}
			} while (isLooping);
		}
		System.out.println();
		String paymentMethod;
		if (customer instanceof MemberCustomer) {
			paymentMethod = Validation
					.validasiInput("Silakan Pilih Metode Pembayaran (Saldo Coin atau Cash)",
							"Pilih (Saldo Coin atau Cash) saja!", "Saldo Koin|saldo koin|Cash|cash");
			System.out.println();
		} else {
			paymentMethod = "Cash";
			System.out.println("Metode Pembayaran");
			System.out.println(paymentMethod);
			System.out.println();
		}

		double total = 0.0;
		for (ItemService itemService : listChoosenItems) {
			total += itemService.getPrice();
		}

		System.out.println();
		if (customer instanceof MemberCustomer) {
			if (paymentMethod.equalsIgnoreCase("Saldo Koin")) {
				if ((((MemberCustomer) customer).getSaldoCoin() - total) < 0) {
					System.out.println("Booking Gagal !!! Saldo Koin Anda Tidak Mencukupi !");
				} else {
					System.out.println("Booking Berhasil !!!");
					newBookingOrder.setBookingId(bookingID);
					newBookingOrder.setCustomer(customer);
					newBookingOrder.setServices(listChoosenItems);
					newBookingOrder.setPaymentMethod(paymentMethod);
					newBookingOrder.setTotalServicePrice(total);
					newBookingOrder.calculatePayment();
					((MemberCustomer) customer).setSaldoCoin(((MemberCustomer) customer).getSaldoCoin() - total);
				}
			} else {
				System.out.println("Booking Berhasil !!!");
				newBookingOrder.setBookingId(bookingID);
				newBookingOrder.setCustomer(customer);
				newBookingOrder.setServices(listChoosenItems);
				newBookingOrder.setPaymentMethod(paymentMethod);
				newBookingOrder.setTotalServicePrice(total);
				newBookingOrder.calculatePayment();
			}
		} else {
			System.out.println("Booking Berhasil !!!");
			newBookingOrder.setBookingId(bookingID);
			newBookingOrder.setCustomer(customer);
			newBookingOrder.setServices(listChoosenItems);
			newBookingOrder.setPaymentMethod(paymentMethod);
			newBookingOrder.setTotalServicePrice(total);
			newBookingOrder.calculatePayment();
		}

		String hrgaService = String.format("%,d", (int) newBookingOrder.getTotalServicePrice());
		String hargaService = hrgaService.replace(',', '.');
		String payment = String.format("%,d", (int) newBookingOrder.getTotalPayment());
		String totalPayment = payment.replace(',', '.');
		String format = "%-20s : %s %n";
		System.out.printf(format, "Total Harga Service", hargaService);
		System.out.printf(format, "Total Pembayaran", totalPayment);
		bookingOrder.add(newBookingOrder);
	}

	// Generate Booking ID
	public static String generateBookingID(Customer customer, List<BookingOrder> bookings) {
		String result = "";
		String custID = customer.getCustomerId();
		String bookNumber = "";
		String temCustID = custID.replaceAll("Cust-", "");
		if (bookings.isEmpty()) {
			bookNumber = "00" + 1;
		} else {
			int tempNumber = bookings.size() + 1;
			if (tempNumber % 10 == 0) {
				bookNumber = "0" + tempNumber;
			} else {
				bookNumber = "00" + tempNumber;
			}
		}
		result = "Book-Cust-" + bookNumber + "-" + temCustID;
		return result;
	}

	// Get All Item Services by Type Vehicle
	public static List<ItemService> getItemServicesByTypeVehicle(String vehicletype,
			List<ItemService> listItemService) {
		List<ItemService> result = new ArrayList<ItemService>();
		for (ItemService itemService : listItemService) {
			if (itemService.getVehicleType().equalsIgnoreCase(vehicletype)) {
				result.add(itemService);
			}
		}
		return result;
	}

	// Get Item Service by ID
	public static ItemService getItemServicebyID(String id, List<ItemService> items) {
		ItemService result = null;
		for (ItemService itemService : items) {
			if (itemService.getServiceId().equalsIgnoreCase(id)) {
				result = itemService;
			}
		}
		return result;
	}

	// Top Up Saldo Koin
	public static void toUpSaldoKoin(Customer customer) {
		System.out.println("=============================================");
		System.out.println("             Top Up Saldo Koin               ");
		System.out.println("=============================================");

		double saldoKoinAwal = ((MemberCustomer) customer).getSaldoCoin();
		double topUp = Validation.numberValidation("Masukan besaran Top Up : ", "Hanya dapat diisi dengan angka saja!");
		((MemberCustomer) customer).setSaldoCoin(((MemberCustomer) customer).getSaldoCoin() + topUp);
		if (saldoKoinAwal != ((MemberCustomer) customer).getSaldoCoin()) {
			System.out.println();
			System.out.println("=============================================");
			System.out.println("     Berhasil Menambahkan Saldo Koin ..!     ");
			System.out.println("=============================================");
			System.out.println();
		}
	}

}
