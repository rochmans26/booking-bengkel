package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.Vehicle;

public class Validation {

	public static String validasiInput(String question, String errorMessage, String regex) {
		Scanner input = new Scanner(System.in);
		String result;
		boolean isLooping = true;
		do {
			System.out.print(question);
			result = input.nextLine();

			// validasi menggunakan matches
			if (result.matches(regex)) {
				isLooping = false;
			} else {
				System.out.println(errorMessage);
			}

		} while (isLooping);

		return result;
	}

	public static int validasiNumberWithRange(String question, String errorMessage, String regex, int max,
			int min) {
		int result;
		boolean isLooping = true;
		do {
			result = Integer.valueOf(validasiInput(question, errorMessage, regex));
			if (result >= min && result <= max) {
				isLooping = false;
			} else {
				System.out.println("Inputan berupa angka dari " + min + " s.d " + max);
			}
		} while (isLooping);

		return result;
	}

	public static int numberValidation(String question, String errorMessage) {
		Scanner input = new Scanner(System.in);
		String inputAngka, regexAngka = "^[0-9]+$";

		boolean isLooping = true;
		do {
			isLooping = true;
			System.out.println(question);
			inputAngka = input.nextLine();
			// input.close();
			if (inputAngka.matches(regexAngka)) {
				isLooping = false;
			} else {
				System.out.println();
				System.out.println("=============================================");
				System.out.println(errorMessage);
				System.out.println("=============================================");
				System.out.println();
			}

		} while (isLooping);
		// input.close();

		return Integer.parseInt(inputAngka);
	}

	public static String customerValidationbyId(String question, String errorMsg, List<Customer> customers) {
		Scanner input = new Scanner(System.in);
		int count = 0;
		String result;
		List<Customer> target = new ArrayList<Customer>();
		boolean isLooping = true;

		do {
			System.out.println(question);
			result = input.nextLine();
			for (Customer customer : customers) {
				if (customer.getCustomerId().equalsIgnoreCase(result)) {
					target.add(customer);
				}
			}
			if (target.isEmpty()) {
				System.out.println();
				System.out.println("=============================================");
				System.out.println(errorMsg);
				System.out.println("=============================================");
				System.out.println();
			} else {
				isLooping = false;
			}
			count++;
			if (count == 3) {
				System.out.println();
				System.out.println(
						"=========================================================================================================");
				System.out.println(
						"Anda Sudah Memasukan Customer ID Yang Salah 3 Kali. Silahkan Hubungi Admin Bengkel Untuk Memulihkan Akun!");
				System.out.println(
						"=========================================================================================================");
				System.out.println();
				isLooping = false;
				MenuService.run();
			}
		} while (isLooping);

		return result;
	}

	public static String vehicleValidationById(String question, String errorMsg, List<Vehicle> vehicles) {
		Scanner input = new Scanner(System.in);
		String result = "";
		boolean isLooping = true;
		List<Vehicle> tempList = new ArrayList<Vehicle>();
		do {
			System.out.println(question);
			result = input.nextLine();
			for (Vehicle vehicle : vehicles) {
				if (vehicle.getVehiclesId().equalsIgnoreCase(result)) {
					tempList.add(vehicle);
				}
			}
			if (tempList.isEmpty()) {
				System.out.println();
				System.out.println("=============================================");
				System.out.println(errorMsg);
				System.out.println("=============================================");
				System.out.println();
			} else {
				isLooping = false;
			}
		} while (isLooping);
		return result;
	}

	public static String ItemServiceValidationById(String question, String errorMsg, List<ItemService> itemServices) {
		Scanner input = new Scanner(System.in);
		String result = "";
		List<ItemService> temp = new ArrayList<ItemService>();
		boolean isLooping = true;
		do {
			System.out.println(question);
			result = input.nextLine();
			for (ItemService itemService : itemServices) {
				if (itemService.getServiceId().equalsIgnoreCase(result)) {
					temp.add(itemService);
				}
			}
			if (temp.isEmpty()) {
				System.out.println();
				System.out.println("=============================================");
				System.out.println(errorMsg);
				System.out.println("=============================================");
				System.out.println();
			} else {
				isLooping = false;
			}

		} while (isLooping);
		return result;
	}

	public static String ItemServiceValidationByIdLainnya(String question, String errorMsg, List<ItemService> allItem,
			List<ItemService> allItemChoosen) {
		Scanner input = new Scanner(System.in);
		boolean isLooping = true;
		String result = "";
		List<ItemService> temp = new ArrayList<ItemService>();
		do {
			System.out.println(question);
			result = input.nextLine();
			for (ItemService itemService : allItem) {
				if (itemService.getServiceId().equalsIgnoreCase(result)) {
					temp.add(itemService);
				}
			}
			if (temp.isEmpty()) {
				System.out.println();
				System.out.println("=============================================");
				System.out.println(errorMsg);
				System.out.println("=============================================");
				System.out.println();
			} else {
				ItemService item = temp.get(0);
				for (ItemService itemService : allItemChoosen) {
					if (itemService.getServiceId().equalsIgnoreCase(item.getServiceId())) {
						System.out.println();
						System.out.println("=============================================");
						System.out.println("Tidak Boleh Memilih Service ID yang sama ..!");
						System.out.println("=============================================");
						System.out.println();
						temp.remove(itemService);
					} else {
						isLooping = false;
					}
				}
			}
		} while (isLooping);
		return result;
	}

	

}
